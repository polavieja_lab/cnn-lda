# LDA-CNN

## Description
  
This is the code for the 2020 preprint [http://arxiv.org/abs/2006.12127](http://arxiv.org/abs/2006.12127):

**Francisco J. H. Heras, Gonzalo G. de Polavieja "Supervised dimensionality 
reduction by a Linear Discriminant Analysis on pre-trained CNN features"**


## Datasets used

This code uses two datasets: 
- MNIST (which is automatically downloaded)
- A butterfly dataset, part of Cuthill, J. F. H., Guttenberg, N., 
Ledger, S., Crowther, R., & Huertas, B. (2019). "Deep learning on butterfly
phenotypes tests evolution’s oldest mathematical model". Science advances, 
5(8), eaaw4967. 

## Directory structure

```
.
├── butterflydata 	# Place data from Cuthill et al. 2019 here.
├── lib 		# Code library of the project 
├── MNIST 		# Dir to be populated by MNIST data
├── models              # Trained CNN-LDA and UMAP models will be saved here 
```

The dataset of Cuthill et al. needs to be downloaded from [driad](https://datadryad.org/stash/dataset/doi:10.5061/dryad.2hp1978)
and placed in the directory butterflydata. See the file `butterflydatatree.txt`
to see where all the files were placed in a working installation.

## Execute

Run `runbutterfly.py` (butterflies) or `runmnist20.py` (MNIST-20). 
The script will start populating models with trained models. 
When finished, it will plot information in different figures.


