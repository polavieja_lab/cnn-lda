import itertools
from multiprocessing import Pool

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.spatial.distance import pdist, squareform

from lib.table import print_latex_table
from lib.umaptest import umap_data
from lib.utils import (load_dataframe_from_conditions_dict, minimum_errors,
                       normalise_df, subsample_pdist, quantify_separation)

MNIST_PAIRS = [('same_digit', 'other')]

def pair_class_centroid(config):
    # We do not need config to classify
    # (The information is in the indices)
    def classify(pair):
        i, j = pair
        if i-j % 10 == 0:
            return 'same_digit'
        else:
            return 'other'
    indices = itertools.combinations(range(20), 2)

    return list(map(classify, indices))


def pair_class_test(config, sampling_index=None):
    dist = pdist(config['labels_test'][:, np.newaxis])
    if sampling_index is not None:
        dist = dist[sampling_index] 
    
    same_class = (dist == 0)
    same_digit = (dist == 10)
    pair_class = ["same_class" if same_class[i] else
                  "same_digit" if same_digit[i] else
                  "other" for i in range(len(same_class))]
    return dict(pair_class=pair_class)

selected_networks = ['mobilenet', 'mnasnet',
                     'googlenet', 'shufflenet', 'squeezenet']
#selected_networks = selected_networks[-1:]

dataset = 'mnist20'
embedding_dim = dict(mnist20=19, mnistmini=9, mnist=9)
cond_d = [
    {'network': selected_networks,  # selected_networks + ['identity'],
     'dataset': [dataset],
     # ['nopca', 200, 500, 1000, 2000, 4000, 6000], #[200, 500, 1000, 2000, 4000], #, 4000, 5000],
     'pca_threshold': ['nopca'],
     'embedding_dim': [embedding_dim[dataset]]},
]

# Include external models for comparison
include_model = ['umap']
# Change options below to show more or less data
show_test = True
max_models_to_consider = None  # If N, consider only the best N models
# Choose the best models using accuracy. If false, centroid shift is used instead
sort_using_accuracy = False
# Correct centroid shift
corrected_centroid_shift = False


df = load_dataframe_from_conditions_dict(cond_d, corrected=corrected_centroid_shift)
# Sorting, decreasing accuracy order
if sort_using_accuracy:
    df_sorted = df.sort_values(by=['accuracy'], ascending=False)
else:
    df_sorted = df.sort_values(by=['mean_centroid_shift'])
df_sorted = df_sorted.iloc[:max_models_to_consider]

# Generating array with all pairwise distances
dist_dict = {}
test_dict = {}
model_names = []
sampling_index = None # First time it is used, it is calculated
for i, row in df_sorted.iterrows():
    key = f'{row.network}_{row.pca_threshold}_{row.layer}'
    model_names.append(key)
    print(
        f"Accuracy of {key} is {row.accuracy:.3f} (in train it was {row.train_accuracy:.3f})")
    print(f"Mean centroid shift of {key} is {row.mean_centroid_shift:.3f}")
    dist_dict[key] = pdist(row.class_centroids)
    test_dict[key], sampling_index = subsample_pdist(pdist(row.coordinates_test),
                                                     sampling_index,
                                                     sampled_len=20000,
                                                     seed=0)
df_sorted.insert(0, 'name', model_names)

# Plotting distances of best network
plt.figure()
print(f"Plotting heatmap of distances between class centroids of {model_names[0]}...")
sns.heatmap(squareform(dist_dict[model_names[0]]), annot=True, linewidths=.5)


config = np.load('config_mnist20.npy', allow_pickle=True).item()
dist_dict['pair_class'] = pair_class_centroid(config)
test_dict.update(pair_class_test(config, sampling_index=sampling_index))

#print('Test dict ', test_dict.keys())
#print('All dict ', dist_dict.keys())

pair_all_df = pd.DataFrame.from_dict(dist_dict)
pair_test_df_sampled = pd.DataFrame.from_dict(test_dict)

accuracies = df_sorted.accuracy.to_numpy()
mean_centroid_shifts = df_sorted.mean_centroid_shift.to_numpy()
pca_thresholds = df_sorted.pca_threshold.to_numpy()

# Adding other models for comparison
for model in include_model:
    if model == 'umap':
        paper_dict = umap_data(dataset=dataset)
    else:
        raise Exception('Unknown model')

    pair_all_df.insert(0, model, paper_dict['dist_centroid'])
    pair_test_df_sampled.insert(0, model, paper_dict['dist_test'][sampling_index])
    accuracies = np.concatenate([[paper_dict['accuracy']], accuracies])
    mean_centroid_shifts = np.concatenate(
        [[paper_dict['mean_centroid_shift']], mean_centroid_shifts])
    pca_thresholds = np.concatenate([[model], pca_thresholds])
    model_names = [model] + model_names


# Normalising pairwise distances by mean
# This is important to compare models
normalise_df(pair_all_df, model_names)
normalise_df(pair_test_df_sampled, model_names)


plt.figure()
dist_corr = pair_all_df.corr()
sns.heatmap(dist_corr, xticklabels=True, yticklabels=True)

g = sns.pairplot(pair_all_df, vars=model_names[:4], hue='pair_class')

pair_all_df.boxplot(by='pair_class', column=model_names[:6])

# Quantify distances between classes of pairs of centroids
number_best_for_table = 8
error_dict = dict(merror1=[], accuracy=accuracies[:number_best_for_table], 
                  model_name=model_names[:number_best_for_table],
                  mean_centroid_shift=mean_centroid_shifts[:number_best_for_table])

merrors = minimum_errors(pair_all_df, model_names, key='centroid',
                         pairs=MNIST_PAIRS, number=number_best_for_table)
error_dict['merror1'] = merrors[0]


if show_test:
    # Box plots for each class
    pair_test_df_sampled.boxplot(by=['pair_class'], column=model_names[:6])
    
    # Pair plots of the best models
    sns.pairplot(pair_test_df_sampled,
                 vars=model_names[:4], hue='pair_class')
    
    # Quantify distances between classes of pairs of test images
    merrors = minimum_errors(pair_test_df_sampled, model_names, 
                             key='test', pairs=MNIST_PAIRS,
                             number=number_best_for_table)
    error_dict['merror1_test'] = merrors[0]
    print_latex_table(error_dict, len(include_model))


plt.show()
