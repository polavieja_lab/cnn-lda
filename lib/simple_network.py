import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models


def load_net(name):
    if name == 'identity':
        return NetId()
    else:
        return NetPT(name)


def obtain_statistics(r):
    results = []
    results.append(torch.max(torch.max(r, dim=1)[0], dim=1)[0])
    results.append(torch.min(torch.min(r, dim=1)[0], dim=1)[0])
    results.append(torch.mean(torch.mean(r, dim=1), dim=1))
    results.append(torch.mean(torch.mean(F.relu(r), dim=1), dim=1))
    results.append(torch.mean(torch.mean(F.relu(-r), dim=1), dim=1))
    result = torch.cat(results, axis=1)
    return result


def obtain_statistics_four_parts(features):
    mid1 = features.shape[1]//2
    mid1b = features.shape[1]//2 - features.shape[1] % 2
    mid2 = features.shape[2]//2
    mid2b = features.shape[2]//2 - features.shape[1] % 2

    parts_r = [features[:, :mid1, :mid2], features[:, :mid1, mid2b:],
               features[:, mid1b:, :mid2], features[:, mid1b:, mid2b:]]
    result = torch.cat([obtain_statistics(r) for r in parts_r], axis=1)
    return result


class NetId(nn.Module):
    """ A convoluted way of defining identity map
    Handy when calculating PCA on pixels
    """

    num_layers = 1

    def forward(self, input, only_layers=None, statistics=None, combine=None):
        return [input]


class NetPT(nn.Module):
    """ Pretrained CNN, cut at different layers to obtain features
    """

    def __init__(self, name='mobilenet', wb_input=False):
        super().__init__()
        if name == 'densenet':
            net = models.densenet161(pretrained=True)
            layers = list(net.features.children())[:-1]
            points = list(range(len(layers)))
        elif name == 'mnasnet':
            net = models.mnasnet1_0(pretrained=True)
            layers = list(net.layers.children())[:-1]
            points = list(range(len(layers)))
        elif name == 'mobilenet':
            net = models.mobilenet_v2(pretrained=True)
            layers = list(net.features.children())[:-1]  # Remove last
            points = list(range(len(layers)))
        elif name == 'squeezenet':
            net = models.squeezenet1_1(pretrained=True)
            layers = list(net.features.children())[:-1]
            points = list(range(len(layers)))  # Avoiding widest layers
        elif name == 'shufflenet':
            net = models.shufflenet_v2_x0_5(pretrained=True)
            layers = list(net.children())[:-2]  # Remove last two
            points = list(range(len(layers)))  # Avoiding widest layers
        elif name == 'googlenet':
            net = models.googlenet(pretrained=True)
            layers = list(net.children())[:-2]  # Remove last two
            points = list(range(len(layers)))
        elif name == 'inception':
            net = models.inception_v3(pretrained=True)
            layers = list(net.children())[:-1]  # Remove last
            points = list(range(len(layers)))
        elif name == 'alexnet':
            net = models.alexnet(pretrained=True)
            layers = list(net.features.children())
            points = list(range(len(layers)))
        elif name == 'vgg':
            net = models.vgg16(pretrained=True)
            layers = list(net.features.children())
            points = list(range(len(layers)))
        elif name == 'resnet18':
            net = models.resnet18(pretrained=True)
            layers = list(net.children())
            points = list(range(len(layers)))
        print(f'{name} sampled at {points}')
        self.enc = nn.ModuleList([nn.Sequential(*layers[points[i]:points[i+1]])
                                  for i in range(len(points)-1)])

    def forward(self, input, only_layers=None, statistics=None, combine=None):
        results = []
        result = input
        for func in self.enc:
            result = func(result)
            results.append(self.results_postprocess(
                result, statistics=statistics))
        if combine == 'all':
            results = [torch.cat(results, axis=1)]
        elif combine == None or combine == 'none':
            pass # The preprint case
        else:
            combine_int = min(int(combine), len(results))
            results = [torch.cat(results[i:i+combine_int], axis=1)
                       for i in range(len(results) - combine_int + 1)]

        if only_layers is not None:
            results = [results[l] for l in only_layers]

        return results

    def results_postprocess(self, result, statistics=False):
        if statistics == 'fourparts':
            return obtain_statistics_four_parts(result)
        if statistics == 'onepart':
            return obtain_statistics(result)
        elif statistics is None:
            # In the preprint we always use this option
            # We simply perform CNN and then flatten the result
            return torch.flatten(result, start_dim=1)  # Reshape here
        else:
            raise Exception('Wrong option for ', statistics)

    @property
    def num_layers(self):
        return len(self.enc) - 1  # Because first layer is not there

