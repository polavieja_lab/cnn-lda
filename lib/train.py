import gc
import time
import warnings
from functools import partial
from multiprocessing import Pool  # as Pool

import numpy as np
import torch
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.metrics import accuracy_score
from torch.utils.data import DataLoader
from tqdm import tqdm

from .loader import loader
from .simple_network import load_net

warnings.filterwarnings('ignore')

BATCH_SIZE = 1000


def mean_across_classes(X, y):
    # scikit-learn/sklern/discriminant_analysis.py
    # classes, y = np.unique(y, return_inverse=True)
    num_classes = y.max() + 1
    cnt = np.bincount(y)
    means = np.zeros(shape=(num_classes, X.shape[1]))
    np.add.at(means, y, X)
    means /= cnt[:, None]
    return means


def pca_params(Xtrain, pca_threshold=1000):
    assert(len(Xtrain.shape) == 2)
    print(f'PCA params for {Xtrain.shape} and theshold {pca_threshold}')
    if min(Xtrain.shape[0], Xtrain.shape[-1]) <= pca_threshold:
        print('Not performing PCA (array smaller than pca_threshold)')
        return None
    else:
        pca = PCA(n_components=pca_threshold).fit(Xtrain)
        print(f'PCA for {Xtrain.shape} produced {pca.components_.shape}')
        return pca


def lda_params(Xtrain, ytrain=None, n_components=None):
    print(f'LDA process, size {Xtrain.shape}')
    clf = LDA(n_components=n_components)
    clf.fit(Xtrain, ytrain)
    return clf

@torch.no_grad()
def use_net(train_data, test_data, network_name, max_total_features=None):
    if torch.cuda.is_available():
        device = torch.device('cuda:0')
    else:
        device = torch.device('cpu')

    print('Loading net...')
    net = load_net(network_name).eval().to(device)

    print('Loading images...')
    dataloader_train = DataLoader(train_data, batch_size=BATCH_SIZE,
                                  shuffle=False)
    num_train = len(train_data)
    print("Num train: ", num_train)
    dataloader_test = DataLoader(test_data, batch_size=BATCH_SIZE,
                                 shuffle=False)

    print('Processing images...')

    def valid_layer(layer):
        return layer[1].shape[1] < max_total_features

    def torch2flat_np(r):
        return r.detach().cpu().flatten(start_dim=1).numpy()

    for i, data in tqdm(enumerate(dataloader_train)):
        x = data[0].to(device)
        if i == 0:
            valid_layers, filtered_y = zip(*filter(valid_layer,
                                                   enumerate(net(x))))
            y_out = [torch2flat_np(r) for r in filtered_y]
            print("Valid layers: ", valid_layers)
        else:
            filtered_y = net(x, only_layers=valid_layers)
            y_out = [np.concatenate([previous_r, torch2flat_np(r)], axis=0)
                     for previous_r, r in zip(y_out, filtered_y)]

    print('Valid layers: ', valid_layers)
    for i, l in enumerate(valid_layers):
        print(f"Layer {l}, shape {y_out[i].shape}, type {y_out[i].dtype}")
    num_features = [y.shape[1] for y in y_out]

    print('Train images finished')

    for i, data in tqdm(enumerate(dataloader_test)):
        x = data[0].to(device)
        filtered_y = net(x, only_layers=valid_layers)
        if i == 0:
            y_out_test = [torch2flat_np(r) for r in filtered_y]
        else:
            y_out_test = [np.concatenate([previous_r, torch2flat_np(r)], axis=0)
                          for previous_r, r in zip(y_out_test, filtered_y)]

    for i, l in enumerate(valid_layers):
        print(
            f"Layer {l}, shape {y_out_test[i].shape}, type {y_out_test[i].dtype}")
    print('Test images finished')

    return y_out, y_out_test, valid_layers, num_features


def train(dataset='mnist', network='mobilenet',  # method='lda', features='',
          pca_threshold=None, embedding_dim=5, max_dims=None,
          max_total_features=''):
    # max_dims are ignored in current implementation
    gc.collect()

    if max_total_features == '':
        max_total_features = np.inf
    train_data, test_data, config = loader(dataset,
                                           scale_for_imagenet=True)
    np.save(f'config_{dataset}.npy', config)

    start = time.time()
    y_out, y_out_test, layers_to_save, num_features = use_net(train_data,
                                                              test_data, network,
                                                              max_total_features=max_total_features)
    print('Network took ', time.time() - start)
    print("We will use the following layers:", layers_to_save)

    if pca_threshold != 'nopca':
        print('Selecting a subset to perform PCA...')
        y_out_pca = []
        for y in tqdm(y_out):
            num_samples_for_pca = min(2*pca_threshold, y.shape[0])
            #print(f"Selecting {num_samples_for_pca} indices of {y.shape[0]}")
            indices = np.random.choice(
                y.shape[0], num_samples_for_pca, replace=False)
            y_out_pca.append(y[indices])

        print('Performing PCA...')
        start = time.time()
        worker = partial(pca_params, pca_threshold=pca_threshold)
        pool = Pool(4)
        params_pca = pool.imap(worker, y_out_pca)
        print('training PCA took ', time.time() - start)

        start = time.time()
        layer_data_after_pca = []
        for i, (layer, pca) in enumerate(zip(layers_to_save, params_pca)):
            if pca is None:
                layer_data_after_pca.append((layer, y_out[i], y_out_test[i]))
            else:
                print(
                    f'Applying pca to {layer}, shapes {y_out[i].shape}, {y_out_test[i].shape}')
                layer_data_after_pca.append((layer, pca.transform(y_out[i]),
                                             pca.transform(y_out_test[i])))

        print('transforming PCA took ', time.time() - start)
        variance_respected_by_pca = 1.0  # To be changed!
    else:
        print('Skipping PCA')
        variance_respected_by_pca = 1.0
        layer_data_after_pca = [(l, y_out[i], y_out_test[i])
                                for i, l in enumerate(layers_to_save)]

    del y_out
    del y_out_test

    print(f"Performing LDA...")
    start = time.time()
    ytrain = train_data.labels[0]
    ytest = test_data.labels[0]
    worker = partial(lda_params, ytrain=ytrain, n_components=embedding_dim)
    #all_res_lda = Pool(2).imap(worker, [l[1] for l in layer_data_after_pca])
    all_res_lda = map(worker, [l[1] for l in layer_data_after_pca])

    print(f'Training LDA took ', time.time() - start)

    start = time.time()
    train_accuracy = []
    accuracy = []
    Xtest_transform = []
    Xtrain_transform = []
    clf_dict = []
    for i, lda in enumerate(all_res_lda):
        layer, Xtrain, Xtest = layer_data_after_pca[i]
        Xtest_transform.append(lda.transform(Xtest))
        Xtrain_transform.append(lda.transform(Xtrain))
        clf_dict_ = {'explained_variance_ratio': lda.explained_variance_ratio_,
                     'priors': lda.priors_, 'scalings': lda.scalings_[:, :lda._max_components],
                     'class_centroids': mean_across_classes(Xtrain_transform[-1], ytrain),
                     'class_centroids_test': mean_across_classes(Xtest_transform[-1], ytest)}
        y_pred = lda.predict(Xtest)
        ytrain_pred = lda.predict(Xtrain)
        accuracy.append(accuracy_score(ytest, y_pred))
        train_accuracy.append(accuracy_score(ytrain, ytrain_pred))
        clf_dict.append(clf_dict_)

    print(f'Performing LDA took ', time.time() - start)

    print("Train ", train_accuracy)
    print("Test ", accuracy)
    output_dict = dict(  # ytrain=ytrain,
        ytest=ytest.numpy(),
        accuracy=accuracy, train_accuracy=train_accuracy,
        num_features=num_features, Xtest_transform=Xtest_transform,
        variance_respected_by_pca=variance_respected_by_pca,
        layers=layers_to_save, clf_dict=clf_dict)

    return output_dict
