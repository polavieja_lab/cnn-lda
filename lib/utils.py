import itertools

import numpy as np
import pandas as pd

from .train import train


def calculate_centroid_shift(class_centroids_train,
                             class_centroids_test,
                             corrected=False,
                             mean=False):
    centroid_shift = (class_centroids_test - class_centroids_train)
    if mean:
        centroid_shift /= centroid_shift.shape[1]
    if corrected:
        std_centroid = np.std(class_centroids_train, axis=0)
        centroid_shift_corrected = centroid_shift / std_centroid[np.newaxis, :]
    else:
        centroid_shift_corrected = centroid_shift
    return np.mean(np.linalg.norm(centroid_shift_corrected, axis=1))

def pair_class_centroid(config):
    """Returns a dictionary with lists of pair classes

    Indices in each list are in the same order of the
    condensed distance matrix from scipy.spatial.distance.pdist:
    (0, 1), (0, 2)... (0, N), (1, 2), (1, 3) ... (N - 1, N)
    where N is the number of centroids (butterfly subspecies)
    """
    pair_class = []
    for i in range(config['num_classes']):
        for j in range(i+1, config['num_classes']):
            if config['comimics_labels'][i] == config['comimics_labels'][j]:
                pair_class.append('comimics')
            elif config['species_labels'][i] == config['species_labels'][j]:
                pair_class.append('same_species')
            else:
                pair_class.append('other')
    return pair_class

def pair_class_test(config, sampling_index=None):
    """Returns a dictionary with lists of pair classes
    
    Indices in each list are in the same order of the
    condensed distance matrix from scipy.spatial.distance.pdist:
    (0, 1), (0, 2)... (0, N), (1, 2), (1, 3) ... (N - 1, N)
    where N is the number of butterfly images
    """
    pair_class = []
    same_sex = []
    same_side = []
    subspecies_test = config['subspecies_labels_test']
    num_test = len(subspecies_test)
    for i in range(num_test):
        for j in range(i+1, num_test):
            # Pair class
            subspecies_i = subspecies_test[i]
            subspecies_j = subspecies_test[j]
            if subspecies_i == subspecies_j:
                pair_class.append('same_subespecies')
            elif config['comimics_labels'][subspecies_i] == config['comimics_labels'][subspecies_j]:
                pair_class.append('comimics')
            elif config['species_labels'][subspecies_i] == config['species_labels'][subspecies_j]:
                pair_class.append('same_species')
            else:
                pair_class.append('other')

            if config['sex_labels_test'][i] == config['sex_labels_test'][j]:
                same_sex.append('yes')
            else:
                same_sex.append('no')

            if config['side_labels_test'][i] == config['side_labels_test'][j]:
                same_side.append('yes')
            else:
                same_side.append('no')
    

    if sampling_index is not None:
        pair_class = [pair_class[i] for i in sampling_index]
        same_sex = [same_sex[i] for i in sampling_index]
        same_side = [same_side[i] for i in sampling_index]

    return dict(pair_class=pair_class, same_sex=same_sex, same_side=same_side)

def set_max_total_features(dataset, pca_threshold):
    """ Set the maximum number of total features

    :param dataset: which dataset we are using
    :param pca_threshold: an integer or 'nopca'
    """
    total_features_no_pca = dict(mnist20=5000, butterfly_tda=15000)
    total_features_pca = dict(mnist20=15000, butterfly_tda=25000)
    if pca_threshold == 'nopca':
        return total_features_no_pca.get(dataset, '')
    else:
        return total_features_pca.get(dataset, '')


def load_condition(condition, corrected=False):
    """ Load results of run with configuration as in `condition`
        
    Checks each configuration, and first tries to load it from disk. 
    Failing that, a new model is trained.
        
    :param condition: A dictionary of configuration
    :param corrected: Boolean, True if we request corrected distances
    """
    net = condition['network']
    dataset = condition['dataset']
    pca_threshold = condition['pca_threshold']
    embedding_dim = condition['embedding_dim']
    max_total_features = set_max_total_features(dataset, pca_threshold)
    filename = f'models/{net}_{dataset}_{max_total_features}_{pca_threshold}_{embedding_dim}.npy'
    try:
        # If already calculated, we load
        loaded = np.load(filename, allow_pickle=True).item()
        print('Loaded from file')
    except:
        # If not precalculated, we calculate and save
        loaded = train(max_total_features=max_total_features, **condition)
        np.save(filename, loaded)
    
    data_dict = []
    for i, layer in enumerate(loaded['layers']):
        data_dict.append(condition.copy())
        data_dict[-1]['layer'] = layer
        for key in ['num_features', 'accuracy', 'train_accuracy']:
            if loaded[key]: #If list is not empty
                data_dict[-1][key] = loaded[key][i]
            else:
                data_dict[-1][key] = np.nan
        data_dict[-1]['train_error'] = 1 - data_dict[-1]['train_accuracy']
        data_dict[-1]['test_error'] = 1 - data_dict[-1]['accuracy']
        class_centroids_train = loaded['clf_dict'][i]['class_centroids']
        class_centroids_test = loaded['clf_dict'][i]['class_centroids_test']
        mean_centroid_shift = calculate_centroid_shift(class_centroids_train,
                                                    class_centroids_test,
                                                    corrected=corrected)
        data_dict[-1]['class_centroids_test'] = class_centroids_test
        data_dict[-1]['class_centroids'] = class_centroids_train
        data_dict[-1]['mean_centroid_shift'] = mean_centroid_shift
        data_dict[-1]['coordinates_test'] = loaded['Xtest_transform'][i]
    return data_dict


def load_dataframe(conditions, corrected=False):
    data_dict = []
    for condition in conditions:
        print(condition)
        data_dict.extend(load_condition(condition,
                                        corrected=corrected))
    return pd.DataFrame(data_dict)

def load_dataframe_from_conditions_dict(cond_d, corrected=False):
    conditions = []
    for d in cond_d:
        conditions += list(map(dict, itertools.product(*[[(k, v) for v in values]
                                                for k, values in d.items()])))
    return load_dataframe(conditions, corrected=corrected)

def subsample_pdist(pdist_array, sampling_index=None, sampled_len=20000, seed=None):
    if sampling_index is None:
        original_len = pdist_array.shape[0]
        print(f"Subsampling {sampled_len} from {original_len}")
        if sampled_len is None: # No subsampling
            sampling_index = np.arange(original_len)
        else:
            if seed is not None:
                np.random.seed(seed)
            sampling_index = np.random.permutation(original_len)[:sampled_len]
    assert (sampled_len is None) or (len(sampling_index) == sampled_len)
    return pdist_array[sampling_index], sampling_index

def normalise_df(df, columns_to_normalise):
    print(columns_to_normalise)
    print(df.keys())
    for column in columns_to_normalise:
        print(f'Normalising {column}')
        df[column] = df[column] / df[column].mean()
        #.apply(lambda x: x / x.mean())

def minimum_error_arrays(x, y):
    # x an array on average smaller than y
    assert len(x.shape) == len(y.shape) == 1
    xy = np.sort(np.concatenate([x, y]))
    xy_mean = (xy[1:] + xy[:-1])/2
    best_error = np.inf
    for z in xy_mean:
        error = np.mean(x > z) + np.mean(y < z)
        best_error = min(error, best_error)
    return best_error


def quantify_separation(df, pair_class_pair, model):
    ''' We quantify separation as the error of the best possible threshold
    '''
    p1, p2 = pair_class_pair
    return minimum_error_arrays(df[df['pair_class'] == p1][model].to_numpy(),
                                df[df['pair_class'] == p2][model].to_numpy())

def minimum_errors_(pair_df, model_names, key='', pair=None, number=10):
    assert pair is not None
    merror = []
    for model in model_names[:number]:
        m = quantify_separation(pair_df, pair, model)
        print(f"In {key}, model {model} has minimum-error {m:.3f} for {pair}")
        merror.append(m)
    return merror


def minimum_errors(pair_df, model_names, key='', pairs=None, number=10):
    ''' We quantify separation as the error of the best possible threshold
    '''
    return [minimum_errors_(pair_df, model_names, key=key, pair=pair, number=number) 
            for pair in pairs]


