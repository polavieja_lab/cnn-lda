import pandas as pd

def print_latex_table(error_dict, num_extra_models):
    keys_to_print = ['model_name', 'accuracy', 'merror1', 'merror1_test']
    df_error_ = pd.DataFrame.from_dict(error_dict)[keys_to_print]
    # Best LDA-CNN comes after the models included for comparison
    best = num_extra_models
    best5 = best + 5
    df_error = df_error_.iloc[:best]
    df_error.loc[best] = ["best"] + list(df_error_.iloc[best, 1:])
    df_error.loc[best+1] = ["mean best 5"] + \
        list(df_error_.iloc[best:best5, 1:].mean(axis=0))
    print(df_error.to_latex(index=False))


