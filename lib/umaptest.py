import itertools

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import umap
from scipy.spatial.distance import pdist, squareform

from .loader import loader
from .train import mean_across_classes
from .utils import (calculate_centroid_shift, minimum_error_arrays,
                    pair_class_centroid, pair_class_test)

sns.set(style='white')#, context='poster')


def train_(dataset='mnist20', embedding_dim=2, n_neighbors=15):
    print('Training ', dataset, embedding_dim, n_neighbors)
    train_data, test_data, config = loader(dataset,
                                        data_augmentation=False,
                                        scale_for_imagenet=True)

    num_points = train_data.data.shape[0]
    data = train_data.data.reshape((num_points, -1))

    target = train_data.labels[0].numpy()
    embedding = umap.UMAP(n_neighbors=n_neighbors, n_components=embedding_dim)
    train_transformed = embedding.fit_transform(data, y=target)

    num_points_test = test_data.data.shape[0]
    data_test = test_data.data.reshape((num_points_test, -1))

    target_test = test_data.labels[0].numpy()
    test_transformed = embedding.transform(data_test)

    return dict(train_transformed=train_transformed, target=target,
                test_transformed=test_transformed, target_test=target_test,
                class_centroids=mean_across_classes(train_transformed, target),
                class_centroids_test=mean_across_classes(test_transformed, target_test))

def train(*args, **kwargs):
    try:
        return train_(*args, **kwargs)
    except np.linalg.LinAlgError:
        return None

def umap_database(dataset, sort_by):
    cond_d = [
        {'dataset': [dataset],
         'embedding_dim': [15, 20, 25, 30, 37],
         'n_neighbors': [10, 15, 20, 25, 30],
        }]

    conditions = []
    for d in cond_d:
        conditions += list(map(dict, itertools.product(*[[(k, v) for v in values]
                                                for k, values in d.items()])))
    data_dict = []
    for condition in conditions:
        print(condition)
        filename = f"models/umap_{condition['dataset']}_{condition['embedding_dim']}_{condition['n_neighbors']}.npy"
        try:
            loaded = np.load(filename, allow_pickle=True).item()
            print('Loaded from file')
        except FileNotFoundError:
            print('No file found')
            loaded = train(**condition)
            np.save(filename, loaded)

        if loaded is None:
            print('Warning, condition not computed')
            continue

        print(loaded.keys())
        class_centroids_train = loaded['class_centroids']
        class_centroids_test = loaded['class_centroids_test']
        mean_centroid_shift = calculate_centroid_shift(class_centroids_train, class_centroids_test)

        data_dict.append(condition.copy())
        data_dict[-1]['class_centroids'] = class_centroids_train
        data_dict[-1]['class_centroids_test'] = class_centroids_test
        data_dict[-1]['mean_centroid_shift'] = mean_centroid_shift
        data_dict[-1]['test_transformed'] = loaded['test_transformed']

    return pd.DataFrame(data_dict).sort_values(by=[sort_by])

def umap_data(dataset='butterfly_tda', sort_by='mean_centroid_shift'):
    df = umap_database(dataset, sort_by=sort_by)
    # Chosing best one
    best_dict = df.iloc[0].to_dict()
    results_dict = dict(dist_test=pdist(best_dict['test_transformed']),
                        dist_centroid = pdist(best_dict['class_centroids']),
                        mean_centroid_shift = best_dict['mean_centroid_shift'],
                        accuracy=0.0)
    return results_dict
