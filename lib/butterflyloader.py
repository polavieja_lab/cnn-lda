'''
Loads data from the article

Jennifer F Hoyal Cuthill, Nicholas Guttenberg, Sophie Ledger, Robyn Crowther, and Blanca Huertas.  Deeplearning on butterfly phenotypes tests evolution’s oldest mathematical model.Science advances, 5(8):eaaw4967,2019

Most of the code is adapted from their published code, as downloaded from:

https://datadryad.org/stash/dataset/doi:10.5061/dryad.2hp1978

We use the following files:

SimplifiedDatabase.csv    # Database of butterfly images
comimics.csv              # Database of butterfly comimic complexes
LowResolution             # Directory containing low res version of butterfly images

All data must be placed in `butterflydata`. See `butterflydatatree.txt` for a tree of
files in a running version of this code.
'''

import csv
import pandas as pd
import numpy as np
from PIL import Image
from sklearn.preprocessing import LabelEncoder

from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import connected_components

# Num of training and test samples in the butterfly paper data
NUM_TRAIN = 1500
NUM_TEST = 968
NUM_SAMPLES = NUM_TRAIN + NUM_TEST


def give_paper_permutation(size=NUM_SAMPLES):
    ''' Gives the permutation of data used in the paper code
        to split train and test data
    '''
    if size != NUM_SAMPLES:
        print('WARNING!!! It seems that you are using paper permutation but not paper data')
    np.random.seed(12345)
    return np.random.permutation(size)


def load_comimics(comimics_file):
    a = list(csv.reader(open(comimics_file, "r"), delimiter=","))
    comimics_matrix = np.array([a_row[1:] for a_row in a[1:]]).astype(np.bool)
    subspecies_names_from_comimics = a[0][1:]
    _, comimics_labels = connected_components(
        csgraph=csr_matrix(comimics_matrix),
        directed=False, return_labels=True)
    return dict(comimics_matrix=comimics_matrix,
                names=subspecies_names_from_comimics,
                labels=comimics_labels)


def load_and_pad_image(imagefile):
    # Adapted from code in paper
    image = np.array(Image.open(imagefile)).transpose(2, 0, 1)
    image_array = np.ones((3, 64, 160), dtype=np.int)*240
    x0 = int((160-image.shape[2])/2)
    image_array[:, :, x0:x0+image.shape[2]] = image
    return image_array


def load_database(database_file, folder):
    index = pd.read_csv(database_file, sep='\t', header=None)

    images = []
    tlabels = []
    species_tlabels = []
    side_tlabels = []
    sex_tlabels = []

    print("Num samples: ", index.shape[0])
    for i in range(index.shape[0]):
        iarr = load_and_pad_image(folder + "/LowResolution/" +
                                  str(index.iloc[i, 1]))

        images.append(iarr)
        tlabels.append(index.iloc[i, 4])
        species_tlabels.append(index.iloc[i, 3])
        side_tlabels.append(index.iloc[i, 2])
        sex_tlabels.append(index.iloc[i, 5])

    images = np.array(images).transpose(0, 2, 3, 1).astype(np.uint8)

    LE_subspecies = LabelEncoder()
    labels = LE_subspecies.fit_transform(tlabels)
    subspecies_names_from_images = list(LE_subspecies.classes_)

    side_labels = LabelEncoder().fit_transform(side_tlabels)
    sex_labels = LabelEncoder().fit_transform(sex_tlabels)

    LE_species = LabelEncoder()
    species_labels_ = LE_species.fit_transform(species_tlabels)

    subspecies_to_species = -np.ones(np.max(labels) + 1)
    for sl, l in zip(species_labels_, labels):
        subspecies_to_species[l] = sl

    images_labels = dict(side=side_labels,
                         sex=sex_labels, subspecies=labels)

    subspecies_labels = dict(names=subspecies_names_from_images,
                             species=subspecies_to_species)

    return images, images_labels, subspecies_labels


def assert_same_names(names1, names2):
    """ Check the list of strings are the same, in the same order"""
    assert(len(names1) == len(names2))
    for n, m in zip(names1, names2):
        n = n.strip()
        m = m.strip()
        assert n == m


def data_loader(folder='data', permute=True):
    database_file = folder + '/SimplifiedDatabase.csv'
    comimics_file = folder + "/comimics.csv"

    comimics = load_comimics(comimics_file)
    images, dataset_labels, subspecies_labels = load_database(
        database_file, folder)
    assert_same_names(comimics['names'], subspecies_labels['names'])

    subspecies_labels = dict(comimics=comimics['labels'],
                             species=subspecies_labels['species'])

    # Permute data and labels
    paper_permutation = give_paper_permutation(images.shape[0])
    if permute:
        images = images[paper_permutation]
        for key in dataset_labels:
            dataset_labels[key] = dataset_labels[key][paper_permutation]

    return images, dataset_labels, subspecies_labels, paper_permutation
