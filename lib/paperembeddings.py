'''
Loads embedding data from the article

Jennifer F Hoyal Cuthill, Nicholas Guttenberg, Sophie Ledger, Robyn Crowther, and Blanca Huertas.  Deeplearning on butterfly phenotypes tests evolution’s oldest mathematical model.Science advances, 5(8):eaaw4967,2019

The file `embedding.csv` must be placed in the directory `butterflydata`
'''

import pandas as pd
from scipy.spatial.distance import pdist, squareform
from . import butterflyloader

def give_embeddings():
    return pd.read_csv("butterflydata/embedding.csv")

def give_mean_embeddings(samples='all'):
    df = give_embeddings()
    if samples != 'all':
        num_samples = df.shape[0]
        ind = butterflyloader.give_paper_permutation(num_samples)
        if samples == 'train':
            df = df.iloc[ind][:butterflyloader.NUM_TRAIN]
        elif samples == 'test':
            df = df.iloc[ind][butterflyloader.NUM_TRAIN:]
        else:
            raise Exception('Wrong option')

    df2 = df[['Subspecies'] + list(map(str, range(1, 65)))]
    df3 = df2.groupby(['Subspecies']).mean()
    return df3

def give_distances_centroids(samples='train'):
    df = give_mean_embeddings(samples=samples)
    print(df.to_numpy())
    return pdist(df.to_numpy())

def give_distances_test():
    df = give_embeddings()
    cols = map(str, range(1, 65))
    num_samples = df.shape[0]
    ind = butterflyloader.give_paper_permutation(num_samples)
    df = df.iloc[ind][butterflyloader.NUM_TRAIN:]
    assert df.shape[0] == butterflyloader.NUM_TEST
    return pdist(df[list(cols)].to_numpy())

def paper_data():
    """Returns a dictionary with:
    
    - Distances between centroids in the training set
    - Distances between images in the test set
    - Mean centroid shift (unused and not calculated)
    - Accuracy of classifier (0.86, as reported in Cuthill et al)

    """
    return dict(dist_centroid=give_distances_centroids(samples='train'),
                dist_test=give_distances_test(),
                mean_centroid_shift=0.0, #TODO
                accuracy=0.86) 


