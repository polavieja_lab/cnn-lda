import numpy as np
from sklearn.model_selection import train_test_split
import torch
from torchvision.datasets import MNIST
from torch.utils.data import Dataset
from torchvision import transforms

from . import butterflyloader


class TTensorDataset(Dataset):
    def __init__(self, data, *labels, transforms=None):
        if transforms is None:
            transforms = []

        if len(data.shape) == 3:
            data = data[..., np.newaxis]

        self.transforms = transforms
        self.data = data
        self.labels = []
        for label in labels:
            if isinstance(label, np.ndarray):
                self.labels.append(torch.from_numpy(label).long())
            else:
                self.labels.append(label)

    def __getitem__(self, index):
        data_tensor = self.data[index]
        for transform in self.transforms:
            data_tensor = transform(data_tensor)
        labels = [l[index] for l in self.labels]
        return [data_tensor] + labels

    def __len__(self):
        return self.data.shape[0]


def load_butterfly(
        scale_for_imagenet=True,
        duplication_by_mirror_sym=False, data_aug_by_translation=False):
    trans = [transforms.ToPILImage(), transforms.ToTensor()]

    if scale_for_imagenet:
        normalise = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                         std=[0.229, 0.224, 0.225])
        trans += [normalise]

    data, labels, subspecies_labels, paper_permutation = butterflyloader.data_loader(
        'butterflydata', permute=True)
    assert data.shape[0] == butterflyloader.NUM_SAMPLES

    data_train, data_test, *label_split = train_test_split(data,
                                                           labels['subspecies'], labels['side'], labels['sex'],
                                                           test_size=butterflyloader.NUM_TEST, shuffle=False)

    labels_train, labels_test, labels_side_train, labels_side_test = label_split[:4]

    if duplication_by_mirror_sym:
        data_train = np.concatenate(
            [data_train, data_train[:, :, ::-1]], axis=0)
        labels_train = np.concatenate([labels_train]*2, axis=0)
        labels_side_train = np.concatenate([labels_side_train]*2, axis=0)

    if data_aug_by_translation:
        data_train = np.concatenate([
            np.roll(data_train, -2, axis=1),
            np.roll(data_train, -1, axis=1), data_train,
            np.roll(data_train, 1, axis=1),
            np.roll(data_train, 2, axis=1),
        ], axis=0)

        data_train = np.concatenate([
            np.roll(data_train, -2, axis=2),
            np.roll(data_train, -1, axis=2), data_train,
            np.roll(data_train, 1, axis=2),
            np.roll(data_train, 2, axis=2),
        ], axis=0)

        labels_train = np.concatenate([labels_train]*5*5, axis=0)
        labels_side_train = np.concatenate([labels_side_train]*5*5, axis=0)

    config = {'size': (64, 156), 'num_channels': data.shape[3],
              'num_classes': int(labels['subspecies'].max()) + 1,
              'subspecies_labels_test': labels_test,
              'species_labels': subspecies_labels['species'],
              'comimics_labels': subspecies_labels['comimics'],
              'side_labels_test': label_split[3],
              'sex_labels_test': label_split[5],
              'conditions': []}
    train_data = TTensorDataset(data_train, labels_train, transforms=trans)
    test_data = TTensorDataset(data_test, labels_test, transforms=trans)
    return train_data, test_data, config


def load_mnist(data_augmentation=True,
               scale_for_imagenet=False, version='normal'):
    train_data = MNIST(root='.', download=True, train=True)
    test_data = MNIST(root='.', download=True, train=False)
    num_classes = 10

    print(train_data.targets)

    if version == 'mini':
        train_data.data = train_data.data[:3000]
        train_data.targets = train_data.targets[:3000]
    elif version == '20':
        train_data.targets[::2] += 10
        test_data.targets[::2] += 10
        num_classes *= 2

    if scale_for_imagenet:
        train_data.data = torch.stack([train_data.data]*3, 1)
        test_data.data = torch.stack([test_data.data]*3, 1)
        N = 3
    else:
        N = 1

    config = {'size': (28, 28), 'num_channels': N, 'num_classes': num_classes,
              'conditions': [], 'labels_test': test_data.targets}
    train_data = TTensorDataset(
        train_data.data.view(-1, N, 28, 28).float() / 255, train_data.targets)
    test_data = TTensorDataset(
        test_data.data.view(-1, N, 28, 28).float() / 255, test_data.targets)
    return train_data, test_data, config


def loader(key, **kwargs):
    if key == 'butterfly':
        return load_butterfly(**kwargs)
    elif key == 'butterfly_tda':
        return load_butterfly(**kwargs, data_aug_by_translation=True)
    elif key == 'butterfly_trda':
        return load_butterfly(**kwargs, data_aug_by_translation=True,
                              duplication_by_mirror_sym=True)
    elif key == 'mnist':
        return load_mnist(**kwargs)
    elif key == 'mnistmini':
        return load_mnist(**kwargs, version='mini')
    elif key == 'mnist20':
        return load_mnist(**kwargs, version='20')
    else:
        raise Exception
